﻿using System.Security.Principal;

namespace TwilioWebhoooooks.Auth
{
    public class BasicAuthenticationIdentity : GenericIdentity
    {
        public string Password { get; set; }

        public BasicAuthenticationIdentity(string name, string password) : base(name, "Basic")
        {
            Password = password;
        }
    }
}