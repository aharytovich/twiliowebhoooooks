﻿using System;
using System.Security.Principal;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Filters;

namespace TwilioWebhoooooks.Auth
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class BasicAuthenticationAttribute : ActionFilterAttribute, IAuthenticationFilter
    {
        public void OnAuthentication(AuthenticationContext filterContext)
        {
            var identity = ParseAuthorizationHeader(filterContext);
            if (identity != null && OnAuthorizeUser(identity.Name, identity.Password))
            {
                var principal = new GenericPrincipal(identity, null);
                filterContext.Principal = principal;
            }
            else
            {
                var host = filterContext.HttpContext.Request.Url?.DnsSafeHost;
                filterContext.HttpContext.Response.Headers.Add("WWW-Authenticate", string.Format("Basic realm=\"{0}\"", host));
                filterContext.Result = new HttpUnauthorizedResult();
            }

            // inside of ASP.NET this is required
            //if (HttpContext.Current != null)
            //    HttpContext.Current.User = principal;

        }

        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
        }

        private BasicAuthenticationIdentity ParseAuthorizationHeader(AuthenticationContext filterContext)
        {
            string authHeader = null;

            var auth = filterContext.HttpContext.Request.Headers["Authorization"];
            if (auth != null && auth.StartsWith("Basic "))
            {
                authHeader = auth.Replace("Basic ", "");
            }

            if (string.IsNullOrEmpty(authHeader))
            {
                return null;
            }

            authHeader = Encoding.Default.GetString(Convert.FromBase64String(authHeader));

            var tokens = authHeader.Split(new[] {':'}, 2, StringSplitOptions.RemoveEmptyEntries);
            if (tokens.Length < 2)
            {
                return null;
            }

            return new BasicAuthenticationIdentity(tokens[0], tokens[1]);
        }

        private bool OnAuthorizeUser(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return false;

            return true;
        }
    }
}