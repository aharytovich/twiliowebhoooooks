﻿using System;
using System.Web.Mvc;
using Twilio.AspNet.Common;
using Twilio.AspNet.Mvc;
using Twilio.TwiML;
using TwilioWebhoooooks.Auth;

namespace TwilioWebhoooooks.Controllers
{
    [BasicAuthentication]
    public class IncomingSmsController : TwilioController
    {
        [HttpPost]
        public TwiMLResult Sms(SmsRequest request)
        {
            // Default message for any incoming message other than handled below
            var responseMessage = "Unknown command. Use HELP for help.";

            switch (request.Body.Trim().ToUpper())
            {
                case "STOP":
                case "CANCEL":
                case "UNSUBSCRIBE":
                case "END":
                case "QUIT":
                    // Add number to the DB
                    responseMessage =
                        "You have been unsubscribed and will no longer receive messages from this number. Reply START to re-subscribe.";
                    break;
                case "START":
                case "YES":
                case "UNSTOP":
                    // remove number from DB
                    responseMessage =
                        "You have been successfully re-subscribed. Reply HELP for help or STOP to unsubscribe.";
                    break;
                case "NAME":
                    responseMessage = $"Hello {HttpContext.User.Identity.Name}!";
                    break;
                case "HELP":
                    responseMessage = "Reply STOP to unsubscribe. Reply START to re-subscribe.";
                    break;
            }

            var response = new MessagingResponse();
            response.Message("A: " +  responseMessage);
            return TwiML(response);
        }

        [HttpPost]
        public TwiMLResult EmptyMessage(SmsRequest request)
        {
            var response = new MessagingResponse();
            response.Message("");
            return TwiML(response);
        }

        [HttpPost]
        public TwiMLResult EmptyObject(SmsRequest request)
        {
            return new TwiMLResult();
        }


        [HttpPost]
        public TwiMLResult Null(SmsRequest request)
        {
            return null;
        }

        [HttpPost]
        public TwiMLResult Exception(SmsRequest request)
        {
            HA();
            var response = new MessagingResponse();
            response.Message("abc");

            return TwiML(response);
        }

        private void HA()
        {
            throw new Exception();
        }
    }
}